// Session -------------------------------------------------
let socket = null
let socketConnected = false
let session = {
    'id': null,
    'key': null,
}

// DOM elements --------------------------------------------
let contentDiv = null
let dataTable = null
let variableTable = null
let windows = {
    'data': null,
    'structure': null,
    'syntax': null,
    'output': null,
}
let bottombarTabs = {
    'data': null,
    'structure': null,
    'syntax': null,
    'output': null,
}

// Data ----------------------------------------------------
let data = null

// Memory --------------------------------------------------
/**
 * For the data window: Shall column headers contain the variable names or the
 * variable labels?
 *
 * Possible values: 'names', 'labels'
 */
let dataWindowNamesLabels = 'names';


// MAIN ========================================================================

function init() {
    // Get DOM variables -----------------------------------
    console.log('hi')
    contentDiv = $('#content')
    dataTable = $('#data-table')
    variableTable = $('#variable-table')
    windows = {
        'data': $('#window-data'),
        'structure': $('#window-structure'),
        'syntax': $('#window-syntax'),
        'output': $('#window-output'),
    }
    bottombarTabs = {
        'data': $('#bottombar-tab-data'),
        'structure': $('#bottombar-tab-structure'),
        'syntax': $('#bottombar-tab-syntax'),
        'output': $('#bottombar-tab-output'),
    }

    // Init DOM --------------------------------------------
    Object.keys(windows).forEach((key) => {
        windows[key].height(contentDiv.height())
    })
    windows.data.css('display', 'block')

    Object.keys(bottombarTabs).forEach((key) => {
        bottombarTabs[key].on('click', bottombarTabClick)
    })

    // WebSocket -------------------------------------------
    session.id = transport_sessionId
    session.key = transport_sessionKey
    let connectionString = `ws://${window.location.host}/ws/desktop/${session.id}/`

    socket = new WebSocket(connectionString)
    connect()
}


function updateData(data_) {
    data = data_

    updateDataTable()
    updateVariableTable()
}


function updateDataTable() {
    // Set column headers ----------------------------------
    let vars = Object.keys(data)
    html = ''
    html += '<tr>'
    html += '   <th></th>'
    vars.forEach((variable) => {
        html += `   <th>${variable}</th>`
    })
    html += '</tr>'

    // Fill with data --------------------------------------
    for (
        let i = 0;
        i < Math.max(...vars.map((v) => data[v].data.length));
        i++
    ) {
        html += '<tr>'
        html += `   <th>${i+1}</th>`
        vars.forEach((v) => {
            html += `   <td dtype="${data[v].type}">`
            html += data[v].data[i]
            html += '   </td>'
        })
        html += '</tr>'
    }
    dataTable.html(html);

    // Make editable ---------------------------------------
    // Uncaught RangeError: too many arguments provided for a function call
    // dataTable.find('td').each((i, td) => {
    //     $(td).attr('contenteditable', 'true')
    //     $(td).on('input', tdOnInput)
    // })
}


function updateVariableTable() {
    // Set column headers ----------------------------------
    html = ''
    html += '<tr>'
    html += '   <th></th>'
    html += '   <th>Name</th>'
    html += '   <th>Type</th>'
    html += '   <th>Label</th>'
    html += '   <th>Missing</th>'
    html += '</tr>'

    // Fill with data --------------------------------------
    let vars = Object.keys(data)
    vars.forEach((v, i) => {
        html += '<tr>'
        html += `   <th>${i + 1}</th>`
        html += `   <td dtype="string">${v}</td>`
        html += `   <td dtype="string">${data[v].type}</td>`
        html += `   <td dtype="string">${data[v].label}</td>`
        html += `   <td dtype="string">${data[v].missing}</td>`
        html += '</tr>'
    })
    variableTable.html(html);

    // Make editable ---------------------------------------
    // Uncaught RangeError: too many arguments provided for a function call
    // variableTable.find('td').each((i, td) => {
    //     $(td).attr('contenteditable', 'true')
    //     $(td).on('input', tdOnInput)
    // })
}


// INTERACTION =================================================================

function tdOnInput(event) {
    let cell = $(event.target)
    console.log('td input:', cell, cell.html())
}


function bottombarTabClick(event) {
    switch ($(event.target).prop('id')) {
        case 'bottombar-tab-data':
            setActiveWindow('data')
            break
        case 'bottombar-tab-structure':
            setActiveWindow('structure')
            break
        case 'bottombar-tab-syntax':
            setActiveWindow('syntax')
            break
        case 'bottombar-tab-output':
            setActiveWindow('output')
            break
        default:
            console.error('Unknown bottom bar id', $(event.target).prop('id'))
    }
}


function setActiveWindow(key) {
    Object.keys(windows).forEach((key) => {
        windows[key].css('display', 'none')
    })
    windows[key].css('display', 'block')
}


function calcTest() {
    socketSend(
        'CALC',
        {
            'type': 'correlation',
            'x': data['bdi_total'].data,
            'y': data['bsi_total'].data,
        }
    )
}


function dataWindowSwitchNamesLabels() {
    switch (dataWindowNamesLabels) {
        case 'names':
            dataWindowNamesLabels = 'labels';
            break;
        case 'labels':
            dataWindowNamesLabels = 'names';
            break;
        default:
            console.error(
                'Invalid value for dataWindowNamesLabels:',
                dataWindowNamesLabels
            )
    }

    console.log(dataWindowNamesLabels)
    updateDataTableHeaders(dataWindowNamesLabels);
}


function updateDataTableHeaders(namesOrLabels) {
    let headers = $(dataTable.find('tr').get(0)).find('th')
    switch (namesOrLabels) {
        case 'names':
            console.log('...names')
            let names = Object.keys(data)
            headers.slice(1).each((i, h) => {
                h.innerHTML = names[i]
            })
            break;
        case 'labels':
            console.log('...labels')
            let labels = Object.keys(data).map((n) => data[n].label)
            headers.slice(1).each((i, h) => {
                h.innerHTML = labels[i]
            })
            break;
        default:
            console.error(
                'Invalid value for namesOrLabels:',
                namesOrLabels
            )
    }
}


// SOCKETS =====================================================================

function socketSend(event, message) {
    socket.send(JSON.stringify({
        'session_id': session.id,
        'session_key': session.key,
        'event': event,
        'message': message,
    }))
}


function connect() {
    socket.onopen = function open() {
        console.log('WebSockets connection created.')
        socketSend(
            "REQUEST DATASET",
            null
        )
    };

    socket.onclose = function (e) {
        console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason)
        setTimeout(function () {
            connect()
        }, 1000)
    };

    // Receive message from server
    socket.onmessage = function (e) {
        let data = JSON.parse(e.data)['payload']
        let message = data['message']
        let event = data['event']
        switch (event) {
            case 'APPEND OUTPUT':
                console.log('received OUTPUT:')
                console.log(message)
                break
            case 'APPEND SYNTAX':
                console.log('received SYNTAX:')
                console.log(message)
                break
            case 'HELLO':
                console.log('received HELLO')
                break
            case 'MIRROR':
                console.log('received MIRROR:')
                console.log(message)
                break
            case 'UPDATE DATASET':
                console.log('UPDATE DATASET')
                updateData(message['data'])
                break
            default:
                console.error('No event')
        }
    };

    if (socket.readyState == WebSocket.OPEN) {
        socket.onopen();
    }
}
