# FOSOSS

Free and Open Source Online Statistics Software / Service.

Psychologie-Studierende lernen Statistik mit SPSS.
SPSS ist teuer und proprietär.
PSPP ist da eine tolle FOSS-Alternative.
Allerdings erfordert PSPP wie PSPP eine lokale Installation, zudem läuft es
unter Windows nicht ganz stabil und schaut nicht sehr schick aus (GTK; unter
Linux ist's dafür sehr ansehlich und unterstützt Themes!).
Um Psychologie-Studierende eine simplere, bequemere Alternative zu SPSS zu
bieten, entwickle ich jetzt also FOSOSS.
Weil das online läuft, ist die Hürde zum Ausprobieren niedrig.
In Aussehen und Verhalten soll FOSOSS ähnlich zu PSPP sein, um kein neues zu
lernendes Pradigma einzuführen (... dafür gibt's schon JASP, R Studio & Co.).


## Ultimative Datensatz-Repräsentation

Datensatz wird für Schnittstellen im JSON- / Objekt-Format wie folgt
repräsentiert.

```
"data": {
    "spalte1": {
        "type": string,
        "label": {...},
        "data": [...],
    },
    "spalte2": {...},
    "spalte3": {...},
    ...
}
```

### `type`

Valide Werte:
- `'string'`
- `'number'`
- `'date'`

### `label`

Valide Werte:
- `'string'`
- `'number'`
- `'date'`

### `data`

Enthält die tatsächlichen Daten dieser Variable als Array.
Repräsentation (insbesondere von floats und datetimes) muss ich noch
spezifizieren.
