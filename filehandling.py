import os
import random
import string

from django.core.files.uploadedfile import TemporaryUploadedFile
import pandas as pd
from pandas.errors import ParserError
import pyreadstat

from fososs.settings import BASE_DIR


TEMP_DIRECTORY = BASE_DIR / 'temp'


def csv_df2dict(
    df: pd.DataFrame,
) -> dict:
    out = {}

    for col in df.columns:
        out[col] = series2col(df[col])

    return out


def dfcol2jsonlist(
    dfcol: pd.Series,
) -> list:
    out = list(dfcol.map(str))
    return out


def sav_df2dict(
    df: pd.DataFrame,
    meta: pyreadstat._readstat_parser.metadata_container,
) -> dict:
    out = {
        col_name: {
            'type': varinfo_pyreadstat2spss(meta.original_variable_types[col_name]),
            'label': col_label,
            'missing': '',
            'data': dfcol2jsonlist(df[col_name]),
        }
        for col_name, col_label
        in zip(
            meta.column_names,
            meta.column_labels,
        )
    }

    return out


def post2dict(
    file: TemporaryUploadedFile,
) -> dict:
    """Convert an uploaded file into a python dict.
    Output Format is the standard data format as specified in README.md

    :returns: None if file cannot be converted
    """

    # Create temp file ------------------------------------
    filename = ''.join([
        random.choice(string.ascii_letters)
        for _ in range(10)
    ]) + file.name

    file_ending = filename[filename.rindex('.') + 1:]

    with open(TEMP_DIRECTORY / filename, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)

    # Read temp file into python --------------------------
    out = None

    # CSV ........................................
    if file_ending in (
        'csv',
        'tsv',
    ):
        df = None
        for sep in (
            ',',
            '\t',
        ):
            try:
                df = pd.read_csv(TEMP_DIRECTORY / filename, sep=sep)
                break
            except ParserError:
                continue
        if df is not None:
            out = csv_df2dict(df)

    # SAV ........................................
    elif file_ending == 'sav':
        df, meta = None, None
        try:
            df, meta = pyreadstat.read_sav(TEMP_DIRECTORY / filename)
        except:
            print('SAV ERROR!')
        if df is not None:
            print(df.head())
            # TODO: Support large datasets
            out = sav_df2dict(df.iloc[:30], meta)

    os.remove(TEMP_DIRECTORY / filename)
    return out


def series2col(
    series: pd.Series,
) -> dict:
    out = {
        'type': 'unknown',
        'label': '',
        'missing': '',
        'data': list(series),
    }

    if series.dtype == 'O':
        out['type'] = 'unknown'
    elif series.dtype == 'int64':
        out['type'] = 'number'

    return out


def varinfo_pyreadstat2spss(
    vartype: str,
) -> dict:
    """ Convert pyreadstat's metadata variable type into my kind of type
        """
    out = ''

    # A -> String ...............................
    # Examples:
    #   A5
    #   A21
    #   A600
    if vartype[0] == 'A':
        return 'string'

    # DATE -> Date ..............................
    # Examples:
    #   DATE11
    #   DATE12
    if vartype[:4] == 'DATE':
        return 'date'

    # EDATE -> Date .............................
    # Examples:
    #   EDATE8
    # TODO: Implement

    # F -> Numeric ..............................
    # Examples:
    #   F8.0
    #   F9.2
    #   F21.10
    if vartype[0] == 'F':
        return 'number'

    # TIME -> Datetime ..........................
    # Examples:
    #   TIME8
    # TODO: Implement

    return 'unknown'
