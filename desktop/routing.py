from django.conf.urls import url
from .consumers import DesktopConsumer


websocket_urlpatterns = [
    url(r'^ws/desktop/(?P<room_code>\w+)/$', DesktopConsumer.as_asgi()),
]