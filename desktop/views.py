from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import redirect, render

from .models import DesktopSession, session_is_valid

import filehandling


def desktop(request, session_id):
    session_id = session_id
    session_key = request.COOKIES.get('session_key')

    if not session_is_valid(session_id, session_key):
        raise Http404('Invalid session')

    args = {
        'session_id': session_id,
        'session_key': session_key,
    }
    return render(request, 'desktop/desktop.html', args)


def start(request):
    if request.method == "POST":
        data = filehandling.post2dict(request.FILES.get('data-file'))
        # if data is None or True:
        #     return redirect('/desktop/start/')
        session = DesktopSession.objects.create(
            data=data,
        )
        session.generate_id_key_set()
        session.save()
        redirect_response = HttpResponseRedirect(
            f"/desktop/desktop/{session.session_id}")
        redirect_response.set_cookie(
            key='session_key',
            value=session.session_key, 
            expires=None,  # lasts as long as clients browser session
        )
        return redirect_response
    return render(request, "desktop/start.html")
