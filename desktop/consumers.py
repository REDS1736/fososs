import json
from asgiref.sync import sync_to_async
from channels.generic.websocket import AsyncJsonWebsocketConsumer

from .models import DesktopSession, session_is_valid


class DesktopConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_code']
        self.room_group_name = 'room_%s' % self.room_name

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, close_code):
        print("Disconnected")
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        """
        Receive message from WebSocket.
        Get the event and send the appropriate event
        """
        response = json.loads(text_data)
        if not await sync_to_async(session_is_valid, thread_sensitive=True)(
            response.get('session_id'),
            response.get('session_key'),
        ):
            print('! INVALID SESSION')
            return

        # Get active DesktopSession
        session = await sync_to_async(
            DesktopSession.objects.get,
            thread_sensitive=True,
        )(
            session_id=response.get('session_id')
        )

        event = response.get("event", None)
        message = response.get("message", None)

        if event == 'REQUEST DATASET':
            await self.channel_layer.group_send(self.room_group_name, {
                'type': 'send_message',
                'event': 'UPDATE DATASET',
                'message': {
                    'data': session.data,
#                    'data': {
#                        'id': {
#                            'type': 'string',
#                            'data': [
#                                'ABC1',
#                                'BCD2',
#                                'CDE3',
#                                'DEF4',
#                                'EFG5',
#                            ],
#                        },
#                        'gender': {
#                            'type': 'string',
#                            'data': [
#                                'm',
#                                'm',
#                                'w',
#                                'w',
#                                'm',
#                            ],
#                        },
#                        'bday': {
#                            'type': 'date',
#                            'data': [
#                                '01.01.1999',
#                                '02.03.1998',
#                                '23.12.2002',
#                                '08.11.2010',
#                                '16.09.1976',
#                            ],
#                        },
#                        'bdi_total': {
#                            'type': 'number',
#                            'data': [
#                                20,
#                                15,
#                                16,
#                                7,
#                                10,
#                            ],
#                        },
#                        'bsi_total': {
#                            'type': 'number',
#                            'data': [
#                                25,
#                                19,
#                                23,
#                                17,
#                                1,
#                            ],
#                        },
#                    },
                },
            })

        if event == 'UPDATE VALUES':
            print('++++++++++++ UPDATE VALUES')
            await self.channel_layer.group_send(self.room_group_name, {
                'type': 'send_message',
                'event': 'MIRROR',
                'message': message,
            })

        if event == 'CALC':
            print(message)
            await self.channel_layer.group_send(self.room_group_name, {
                'type': 'send_message',
                'event': 'APPEND OUTPUT',
                'message': 'blablabla output',
            })
            await self.channel_layer.group_send(self.room_group_name, {
                'type': 'send_message',
                'event': 'APPEND SYNTAX',
                'message': 'blablabla syntax',
            })


    async def send_message(self, res):
        """ Receive message from room group """
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            "payload": res,
        }))
