from django.urls import path
from .views import desktop, start

urlpatterns = [
    path('desktop/<session_id>', desktop),
    path('start/', start),
]
