from django.contrib import admin

from desktop.models import DesktopSession


admin.site.register(DesktopSession)
