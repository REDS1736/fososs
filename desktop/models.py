import random
import string

from django.core.exceptions import ObjectDoesNotExist
from django.db import models


# UTILITY ======================================================================

def generate_random_numstring(length: int) -> str:
    allowed_chars = string.ascii_letters
    out = ''.join([
        random.choice(allowed_chars)
        for _
        in range(length)
    ])
    return out


def session_is_valid(session_id: str, session_key: str) -> bool:
    try:
        session = DesktopSession.objects.get(session_id=session_id)
    except ObjectDoesNotExist:
        return False

    if session.session_key != session_key:
        return False

    return True


# MODELS =======================================================================

class DesktopSession(models.Model):
    session_id = models.CharField(
        verbose_name="Session ID",
        max_length=32,
        null=False,
    )
    session_key = models.CharField(
        verbose_name="Session Key",
        max_length=32,
        null=False,
    )
    data = models.JSONField(
        verbose_name="Data",
        null=False,
    )

    def __str__(self):
        return f"{self.session_id} ({self.session_key})"

    def generate_id_key_set(self):
        self.session_id = generate_random_numstring(32)
        self.session_key = generate_random_numstring(32)

    def info(self):
        print(f"ID:  {self.session_id}")
        print(f"Key: {self.session_key}")
        print(f"Variables: {list(self.data.keys())}")
